Calculate end to end delay and jitter QoS metrics                               
-----------------------                                                         
                                                                                
Part of Networking service CSC 576 project to calculate QoS metrics: end-to-end 
delay and jitter over two hosts connected over the internet for peak and non 
peak hours (Networking Services QoS, Signaling and Processes by Dr. Harry 
Perros, Chap 4 - Problem 2).                    

Steps
===========================
 1. The procedures uses [scapy][1] packet manipulation program to                   
generate/record the packets; and [numpy][2] python packages for the histogram.  
 2. The sender host is an Ubuntu 13.04 machine while the receiver is a remote 
[VCL][3] Ubuntu 12.04 VM. 
 3. Install the `scapy` package on both sender and receiver                           
`sudo apt-get install scapy`                                                      
 4. Update the time on both sender and receiver since the procedure uses global 
clock to calculate the delay
`sudo ntpdate ntp.ubuntu.com`                                                        
 5. The receiver listens for UDP packets and uses the filter to match the 
received packets using `scapy::sniff()` 

        ddchavan@bn20-113:~$ sudo scapy                                     
        INFO: Can't import python gnuplot wrapper . Won't be able to plot.   
        INFO: Can't import PyX. Won't be able to use psdump() or pdfdump().  
        WARNING: No route found for IPv6 destination :: (no default route?) 
        Welcome to Scapy (2.2.0)                                             
        >>> f = open('trace', 'w')                                            
        >>> sniff(iface = "eth1", filter="udp and (port 8000 or port 9000)", \
        count = 1100, \                                                       
        prn=lambda x: f.write(x.sprintf("%IP.id% %IP.src%:%UDP.sport% -> " +  
        "%IP.dst%:%UDP.dport% - %UDP.payload% - " +                           
        "%time%") + '\n'))                                                    
        ^C<Sniffed: TCP:0 UDP:1000 ICMP:0 Other:0>                            
        >>> f.close()                                                         
        >>>                

 6. The sender host send the UDP packets using `scapy::send()` present in `send_n.py`

        devendra@deven-T530:~$ sudo scapy
        INFO: Can't import python gnuplot wrapper . Won't be able to plot.
        INFO: Can't import PyX. Won't be able to use psdump() or pdfdump().
        WARNING: No route found for IPv6 destination :: (no default route?)
        Welcome to Scapy (2.2.0)
        >>> import sys, os
        >>> sys.path.append(os.environ['HOME'])
        >>> from send_n import send_n
        >>> send_n(dst_ip='152.46.20.113', n=1000)
        Sent 1 packets.
        .
        Sent 1 packets.
        .
        ...
        .
        Sent 1 packets.
        .
        >>>    
        

 [1]: http://www.secdev.org/projects/scapy/                                     
 [2]: http://www.numpy.org/   
 [3]: http://vcl.ncsu.edu                                                  
                                  
