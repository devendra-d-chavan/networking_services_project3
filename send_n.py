#!/usr/bin/python

"""                                                                             
Contains method to send n UDP packets using scapy::send()
                                                                                
Requires scapy package                                    
    sudo apt-get install scapy                            
"""                                                                             
                                                                                
__author__ = "Devendra D. Chavan"                                               
__email__  = "ddchavan@ncsu.edu"  

def send_n(dst_ip, n = 100, src_port = 8000, dst_port = 9000, interval = 0.5):
    """Sends an UDP packet n times to the destination ip address

    Args:
        dst_ip:     IP address of the destination
        n:          Number of packets to be sent
        src_port:   Source UDP port         
        dst_port:   Destination port
        interval:   Time interval between subsequent packets
    """

    from datetime import date, datetime

    for i in range(n):
        send(IP(dst=dst_ip, id = i)/\
                UDP(sport = [src_port], dport=[dst_port])/\
                (date.strftime(datetime.now(), '%H:%M:%S.%f') + ' '*3), 
            inter=interval)
