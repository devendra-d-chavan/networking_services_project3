#!/usr/bin/python

"""
Processes the scapy trace file to generate the end-to-end delay and jitter QoS 
parameters

Generates the mean, 95 percentile and histogram of the end-to-end delay and 
jitter values.

Requires numpy and matplotlib python modules 
    sudo apt-get install python-numpy python-matplotlib 
"""

__author__ = "Devendra D. Chavan"
__email__  = "ddchavan@ncsu.edu"

def save_histogram(arr, filepath, title, xlabel, num_bins = 100):
    """Plots and saves the histogram

    Args:
        arr:        Array for containing the value to be plotted
        filepath:   Path where the histogram is to be saved (saved as pdf)
        title:      Title of the histogram
        xlabel:     Label to be shown in the x axis 
        num_bins:   Number of bins to use for generating the histogram
    """
    import matplotlib.pyplot as plt
    
    fig = plt.figure()
    ax = fig.add_subplot(1, 1, 1)
  
    ax.hist(arr, num_bins, color='blue', alpha=0.7)
    plt.title(title) 
    plt.xlabel(xlabel)
    plt.ylabel('Count')
    plt.savefig(filepath, format='pdf')

def get_jitter(e2e_delays):
    """Retrieves the jitter for the given end delays

    Calculated as jitter[i] = e2e_delay[i] - e2e_delay[i-1], using numpy.diff()

    Args:
        e2e_delays:     End to end delays

    Returns:
        The jitter values
    """
    import numpy as np

    jitter = np.diff(e2e_delays)
    return jitter

def get_percentile(arr, per = 95):
    """Returns the percentile score for the given values 

    Calculated as value at sorted_arr[0.95*n], n = len(arr)

    Args:
        arr:    Array containing the values
        per:    Percentile value

    Returns:
        The percentile score
    """
    import math

    sorted_arr = sorted(arr)
    percentile = sorted_arr[int(math.ceil(per*len(arr)/100)) - 1]
    return percentile 

def get_end2end_delays(send_recv_times):
    """Returns the end to end delays in seconds for the given send/recv times 

    Args:
        send_recv_times:    List of send and receive times

    Returns:
        The end to end delays 
    """
    e2e_delays = list()
    for (_, send_time, recv_time) in send_recv_times:
        delay = (recv_time - send_time).total_seconds()
        e2e_delays.append(delay)

    return e2e_delays

def get_max_end2end_diff(e2e_delays):
    """Returns the difference between the maximum and minimum end to end delays 
       in seconds  

    Args:
        e2e_delays: List of end to end delays in seconds

    Returns:
        The range of end to end delays 
    """
    range_ = max(e2e_delays) - min(e2e_delays)
    return range_

def get_packet_loss(send_recv_times, n):
    """Returns the packet loss percentage

    Defined as (num(sent_packets) - num(recv_packets)) * 100/num(sent_packets)
    Args:
        send_recv_times:    List of send and receive times
        n:                  Number of packets sent

    Returns:
        Packet loss percentage
    """
    # Verify whether the ids are in sequence and count missing ids (starting from zero)
    send_ids = set(range(n))
    recv_ids = set([id_ for id_, _, _ in send_recv_times])
    packet_loss = len(send_ids - recv_ids) * 100/len(send_ids)

    return packet_loss


def tokenize_trace(filepath):
    """Tokenizes the trace file into id, send_time, recv_time
    
    Each line conforms to the format:
    <id> <src ip>:<src port> -> <dst ip>:<dst port> - <send time> - <recv time>

    Args:
        filepath:   Path of the trace file

    Returns:
        The send receive times 
    """
    from datetime import datetime

    send_recv_times = list()
    with open(filepath) as f:
        for line in f:
            # line: 0 174.109.240.33:8000 -> 152.46.20.113:9000 - 03:34:02.722774    - 03:34:02.69889
            times = line.rsplit('-', 2)

            send_time = datetime.strptime(times[len(times) - 2].strip(), 
                                            '%H:%M:%S.%f')
            recv_time = datetime.strptime(times[len(times) - 1].strip(), 
                                            '%H:%M:%S.%f')
            id_ = int(line.split(' ')[0])

            send_recv_times.append((id_, send_time, recv_time))

    return send_recv_times      

def get_peak_non_peak_names(filepath):
    """Returns the peak and non peak file paths for the given filepath
    
    Appends the suffix _peak or _non_peak to each filename

    Args:
        filepath:   Base filepath

    Returns:
        tuple containing the peak and non peak suffixed file paths
    """
    import os

    peak_suffix = 'peak'
    non_peak_suffix = 'non_peak'

    (filename, extension) = os.path.splitext(filepath)

    peak_filename = filename + '_' + peak_suffix + extension
    non_peak_filename = filename + '_' + non_peak_suffix + extension

    return (peak_filename, non_peak_filename)

def process_trace(input_filepath, e2e_delay_hist_filepath, 
                    jitter_hist_filepath, n):
    """Processes the given trace 
    
    Generates the histogram and prints the mean & 95 percentile values for the 
    end to end delay and jitter

    Args:
        input_filepath:             receiver trace file path
        e2e_delay_hist_filepath:    end to end delay histogram filepath
        jitter_hist_filepath:       jitter histogram filepath
    """
    import numpy as np

    send_recv_times = tokenize_trace(input_filepath)

    e2e_delays = get_end2end_delays(send_recv_times)
    save_histogram(e2e_delays, e2e_delay_hist_filepath, 
                    'End to end delays histogram', 'Delay (sec)')

    print 'Mean end to end delay : {0} sec, 95 percentile at: {1} sec\n'.format(
                    np.mean(e2e_delays), get_percentile(e2e_delays))

    jitter = get_jitter(e2e_delays)
    save_histogram(jitter, jitter_hist_filepath, 'Jitter histogram', 
                    'Jitter (sec)')

    print 'Mean jitter : {0} sec, 95 percentile at: {1} sec\n'.format(np.mean(jitter), 
                                                    get_percentile(jitter))

    print 'Difference between smallest and largest end to end delay : {0} sec\n'.format(
                                            get_max_end2end_diff(e2e_delays))

    print 'Packet loss : {0}%\n'.format(get_packet_loss(send_recv_times, n))

def main(argv):
    import getopt
    import os

    # Set the default values
    input_filepath = ''
    e2e_delay_hist_filepath = 'end_to_end_delay_hist.pdf'
    jitter_hist_filepath = 'jitter_hist.pdf'
    opts, _ = getopt.getopt(argv,"i:e2e:jitter:n:")
    n = 100

    for opt, arg in opts:
        if opt == '-i':
            input_filepath = arg
        elif opt == '-e2e':
            e2e_delay_hist_filepath = arg
        elif opt == '-jitter':
            jitter_hist_filepath = arg
        elif opt == '-n':
            n = int(arg)

    peak_input_filepath, non_peak_input_filepath = get_peak_non_peak_names(input_filepath)
    peak_e2e_delay_hist_filepath, non_peak_e2e_delay_hist_filepath = \
                            get_peak_non_peak_names(e2e_delay_hist_filepath)

    peak_jitter_hist_filepath, non_peak_jitter_hist_filepath = \
                            get_peak_non_peak_names(jitter_hist_filepath)
    print '\nNon peak time results\n'
    process_trace(non_peak_input_filepath, non_peak_e2e_delay_hist_filepath, 
                    non_peak_jitter_hist_filepath, n)

    print '\n\nPeak time results\n'
    process_trace(peak_input_filepath, peak_e2e_delay_hist_filepath, 
                    peak_jitter_hist_filepath, n)

    
if __name__ == "__main__":
    import sys
    main(sys.argv[1:])
